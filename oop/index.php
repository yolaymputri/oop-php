<?php


require_once ('animal.php');
require_once('frog.php');
require_once('ape.php');

$animals= new animal("shaun");

echo "Name = ".$animals->name."<br>";
echo "Legs = ".$animals->legs. "<br>";
echo "Cold blooded = ".$animals->cold_blooded. "<br> <br> <br>";

$animals2= new frog ("buduk");

echo "Name = ".$animals2->name."<br>";
echo "Legs = ".$animals2->legs. "<br>";
echo "Cold blooded = ".$animals2->cold_blooded. "<br>";
echo $animals2->jump(). "<br> <br> <br>";


$animals3= new ape ("kera sakti");

echo "Name = ".$animals3->name."<br>";
echo "Legs = ".$animals3->legs. "<br>";
echo "Cold blooded = ".$animals3->cold_blooded. "<br>";
echo $animals3->yell(). "<br>";
